"           _
"    __   _(_)_ __ ___  _ __ ___
"    \ \ / / | '_ ` _ \| '__/ __|
" _   \ V /| | | | | | | | | (__
"(_)   \_/ |_|_| |_| |_|_|  \___|
"""""""""""""""""""""""""""""""""

"""""""""""
"" General:
"""""""""""


set ignorecase                  " Searching is case insensitive
set hls is                      " Update search as you type
set backspace=indent,eol,start  " Make backspace work like most other apps
set number                      " Precede each line with its line number
set bg=dark                     " For dark background terminal
"set bg=                        " For light background terminal
set showmatch                   " Show matching bracket
set autoindent                  " Copy indent from current line when starting a new line
set smartindent                 " Smart autoindenting when starting a new line
set tabstop=4                   " Number of spaces that a <Tab> in the file counts
set shiftwidth=4                " Number of spaces to use for each step of (auto)indent
set expandtab                   " Use the appropriate number of spaces to insert a <Tab>
se laststatus=2                 " Always show the status line

" Better multiple lines tab indentation
vnoremap < <gv
vnoremap > >gv

" Clean search highlight
nnoremap <silent> <C-l> :nohl<CR>

"Toggle between paste and nopaste
nnoremap <c-i> <Esc>:set paste!<cr>

" Toggle between ``number`` and ``nonumber``
nnoremap <c-n> <Esc>:set nu!<cr>
inoremap <c-n> <Esc>:set nu!<cr>

" Toggle cursor line/column highlight 
nnoremap <c-h> <Esc>:se cursorline!<cr>:se cursorcolumn!<cr>
inoremap <c-h> <Esc>:se cursorline!<cr>:se cursorcolumn!<cr>

" Folding
set nofoldenable                " Unfolds everything when open file
"set foldmethod=manual          " All folds must be typed
set foldmethod=indent           " The rule for folding is indentation
"set foldmethod=syntax          " The rule for folding is the syntax
set foldnestmax=30
set foldlevel=1

" XML folding
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax

" Disable audio/visual bells
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Disable pageup/pagedown by presing <S-Up/Down> in V-LINE mode
vmap <S-Up> <Nop>
vmap <S-Down> <Nop>

" Leader key
let mapleader=","

""""""""""""""
"" Navigation:
""""""""""""""

" Buffers
set hidden
nnoremap <A-Right> <Esc>:bnext<cr>
nnoremap <A-Left> <Esc>:bprevious<cr>
nnoremap <A-Up> <Esc>:b#<cr>


" New tab
nnoremap <c-t> :tabnew<cr>
inoremap <c-t> <esc>:tabnew<cr>

" Navigate to the previous window tab
nnoremap <F3> <Esc>:tabp<CR>
inoremap <F3> <Esc>:tabp<CR>
nnoremap <S-Left> <Esc>:tabp<CR>
inoremap <S-Left> <Esc>:tabp<CR>

" Navigate to the next window tab
inoremap <F4> <Esc>:tabn<CR>
nnoremap <F4> <Esc>:tabn<CR>
nnoremap <S-Right> <Esc>:tabn<CR>
inoremap <S-Right> <Esc>:tabn<CR>

" Move tab left
nnoremap <C-S-Left> :tabm -1<cr>
inoremap <C-S-Left> <esc>:tabm -1<cr>

" Move tab right
nnoremap <C-S-Right> :tabm +1<cr>
inoremap <C-S-Right> <esc>:tabm +1<cr>

" Navigate to the next vertical split
nnoremap <S-Up> <Esc><C-w><Left>
inoremap <S-Up> <Esc><C-w><Left>
nnoremap <F5> <Esc><C-w><Left>
inoremap <F5> <Esc><C-w><Left>

" Navigate to the previous vertical split
nnoremap <S-Down> <Esc><C-w><Right>
inoremap <S-Down> <Esc><C-w><Right>
nnoremap <F6> <Esc><C-w><Right>
inoremap <F6> <Esc><C-w><Right>

" Scroll up screen
nnoremap <C-Up> <C-y>
vnoremap <C-Up> <C-y>

" Scroll down screen
nnoremap <C-Down> <C-e>
vnoremap <C-Down> <C-e>


"""""""""""
"" Plugins:
"""""""""""

" Pathogen (Make installation of plugins easy)
execute pathogen#infect()
syntax on
filetype plugin indent on


" Tagbar (Display tags in a window, ordered by class)
nmap <F2> :TagbarToggle<CR>


" Undotree
nnoremap <F5> <Esc>:UndotreeToggle<cr>
inoremap <F5> <Esc>:UndotreeToggle<cr>

if has("persistent_undo")
    set undodir=~/.undodir/
    set undofile
endif

" Airline
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1


" CtrlP (Fuzzy finder)
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_map = '<c-d>'
"nnoremap <c-d> :CtrlP<cr>
"inoremap <c-d> <Esc>:CtrlP<cr>
let g:ctrlp_open_multiple_files = 'ij'

" NERDTree
nnoremap <c-e> <Esc>:NERDTreeToggle<cr>
inoremap <c-e> <Esc>:NERDTreeToggle<cr>


map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>


"""""""""""""
"" Highlight:
"""""""""""""

" Matching parenthesis
hi MatchParen cterm=underline,bold ctermbg=black ctermfg=red

" Tab background
hi TabLineFill term=bold cterm=bold ctermbg=0

" Line numbers
hi LineNr ctermfg=grey

" Vertical split vertical bar
:hi VertSplit ctermfg=black ctermbg=black

" Python
hi pythonString ctermfg=5 cterm=none guifg=2
hi pythonBuiltin ctermfg=27 cterm=none guifg=2
hi pythonFunction ctermfg=14 cterm=none guifg=2
hi pythonExceptions ctermfg=2 cterm=none guifg=2
hi pythonString ctermfg=13 cterm=none guifg=2
hi pythonInclude ctermfg=11 cterm=none guifg=2

" C/C++
hi cType ctermfg=2 cterm=none guifg=2
hi cStructure ctermfg=118 cterm=bold guifg=2
hi cInclude ctermfg=4 cterm=none guifg=2
hi cDefine ctermfg=32 cterm=bold guifg=2
hi cPreCondit ctermfg=32 cterm=bold guifg=2
hi cCppString ctermfg=9 cterm=none guifg=2
hi cString ctermfg=9 cterm=none guifg=2

