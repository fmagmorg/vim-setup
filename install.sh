#!/bin/bash

sudo echo ''

if [[ -d ~/.vim || -f ~/.vimrc ]]
then
    echo '[E] ~/.vim or ~/.vimrc already exist. Aborting installation.'
    echo '[I] Backup your current vim setup and remove both ~/.vim and ~/.vimrc'
else
    echo '[I] VIM Setup'
    echo '[I] + pathogen (vim plugins)'
    sudo apt-get install -yqq curl
    mkdir -p ~/.vim/autoload ~/.vim/bundle && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    cp vimrc ~/.vimrc

    echo '[I] + tagbar'
    sudo apt-get install -yqq exuberant-ctags
    git clone https://github.com/majutsushi/tagbar.git ~/.vim/bundle/tagbar.git

    echo '[I] + jedi-vim'
    sudo apt-get install -yqq python-pip
    sudo pip install jedi
    git clone https://github.com/davidhalter/jedi-vim.git ~/.vim/bundle/jedi-vim

    echo '[I] + airline'
    git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline

    echo '[I] + syntastic'
    git clone https://github.com/scrooloose/syntastic.git ~/.vim/bundle/syntastic.git

    echo '[I] + MatchTagAlways'
    git clone https://github.com/Valloric/MatchTagAlways.git ~/.vim/bundle/MatchTagAlways.git

    echo '[I] + Undotree'
    git clone https://github.com/mbbill/undotree.git ~/.vim/bundle/undotree.git
    mkdir ~/.undodir

    echo '[I] + ctrlp'
    git clone https://github.com/kien/ctrlp.vim.git ~/.vim/bundle/ctrlp.vim.git

    echo '[I] + NERDTree'
    git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree.git

    echo '[I] + Vagrant syntax'
    git clone https://github.com/hashivim/vim-vagrant.git ~/.vim/bundle/vim-vagrant.git
fi


if [[ -f ~/.screenrc ]]
then
    echo '[E] ~/.screenrc already exists. Aborting installtion.'
    echo '[I] Backup your current screen setup and remove ~/.screenrc'
else
    echo '[I] Screen Setup'
    echo '[I] + screen'
    sudo apt-get install -yqq screen
    cp screenrc ~/.screenrc
fi


